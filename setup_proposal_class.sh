#!/bin/bash

if [ ! -d proposal ]
then
    mkdir proposal
fi

for d in dfg eu base etc
do
    cp -uvr LaTeX-proposal/$d proposal
    cd proposal/$d
    rm -rvf examples
    latex *.ins
    cd -
done

export TEXINPUTS=$PWD/proposal//:$TEXINPUTS
