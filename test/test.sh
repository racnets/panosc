#!/bin/bash

oldpwd=$PWD
cd /tmp
if [ -d panosc ]
then
  rm -rvf panosc
fi

git clone git@bitbucket.org:racnets/panosc
cd panosc
source setup_proposal_class.sh
cd src
latexmk -pdf draft.tex
sort draft.delivs > draft.deliverables
latexmk -pdf -g draft.tex
xdg-open draft.pdf
cd $oldpwd
